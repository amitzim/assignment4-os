#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define KB			1024
#define SMALL	(6 * KB)
#define MEDIUM	(70 * KB)
#define LARGE	(KB * KB)
#define O_RDONLY  0x000
#define O_WRONLY  0x001
#define O_RDWR    0x002
#define O_CREATE  0x200
#define O_SYMLINK 0x020




void file_size_test(){
	int fd = -1;
	int bytes_read = -1;
	fd = open("./check.txt", O_CREATE | O_RDWR);
	if(fd < 0){
		printf(1, "failed to open file\n");
		exit();
	}
	char *ptr_small = (char *)malloc(SMALL);
	// printf(1, "%d\n", );
	memset(ptr_small, 0x31, SMALL - 1);	
	if((bytes_read = write(fd, ptr_small, SMALL)) != SMALL){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	free(ptr_small);
	
	printf(1, "Finished writing 6KB (direct)\n");
	char *ptr_med = (char *)malloc(MEDIUM - SMALL);
	memset(ptr_med, 0x32, MEDIUM - SMALL - 1);
	if((bytes_read = write(fd, ptr_med, MEDIUM - SMALL)) != (MEDIUM - SMALL)){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	printf(1, "Finished writing 70KB (single indirect)\n");
	free(ptr_med);

	char *ptr_large = (char *)malloc(LARGE - MEDIUM);
	memset(ptr_large, 0x33, LARGE - MEDIUM - 1);
	if((bytes_read = write(fd, ptr_large, LARGE - MEDIUM)) != (LARGE - MEDIUM)){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	printf(1, "Finished writing 1MB (single indirect)\n");
	free(ptr_large);

	close(fd);
}

void check_tags(){
	char buf [30];
	int fd = open("gg",O_RDWR|O_CREATE);
	int fd2 = open("ez",O_RDWR|O_CREATE);
	//int fd2 = open("amit",O_RDWR|O_CREATE);

	if (ftag(fd2,"Tyrion","Lannister") == -1){
		printf(1,"check_tags:Error occured! in ftag\n");
	}

	if (gettag(fd2,"Tyrion",buf) == -1){
		printf(1,"check_tags:Error occured! in gettag\n");
	}

	if (funtag(fd2,"Tyrion") == -1){
		printf(1,"check_tags:Error occured! in gettag\n");
	}

	if (gettag(fd2,"Tyrion",buf) == -1){
		printf(1,"check_tags:Tyrion we are going to see if you really shit gold\n");
	}

	if (ftag(fd,"Nedd","hand of the king") == -1){
		printf(1,"check_tags:Error occured! in ftag\n");
	}
	printf(1,"finished TAGGING, now going to get this tag... \n ");
	if (ftag(fd,"Robb","king in the north") == -1){
		printf(1,"check_tags:Error occured! in ftag\n");
	}
	const char* ch = "Nedd";
	if (gettag(fd,ch,buf) == -1){
		printf(1,"check_tags:Error occured! in gettag\n");
	}
	printf(1,"value of %s is %s\n",ch,buf);

	const char* ch2 = "Robb";
	if (gettag(fd,ch2 ,buf) == -1){
		printf(1,"check_tags:Error occured! in gettag\n");
	}
	printf(1,"value of %s is %s\n",ch2,buf);

	printf(1,"finished TAGGING, now going to kill the king in the north... \n ");

	if (funtag(fd,ch2) == -1){
		printf(1,"check_tags:Error occured! in funtag\n");
	}
	if (gettag(fd,ch2 ,buf) == -1){
		printf(1,"check_tags: there is no king in the north muahahahah\n");
	}

	if (funtag(fd,ch) == -1){
		printf(1,"check_tags:Error occured! in funtag\n");
	}

	if (gettag(fd,ch,buf) == -1){
		printf(1,"check_tags: Nedd watch your head... amm oops\n");
	}

}


int
main(int argc, char *argv[]){
	//file_size_test();
	check_tags();
	exit();
}