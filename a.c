#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"

#define BUFSIZE		14
#define NUM_TESTS	4
#define STDOUT		1

#define KB (1 << 10)
#define SIX 6 
#define SEVENTY 70
#define STDOUT 1

#define KB1			1024
#define SMALL	(6 * KB1)
#define MEDIUM	(70 * KB1)
#define LARGE	(KB * KB1)

void check_tags2(){
	int fd = open("sanityFile", O_RDWR|O_CREATE);

	printf(STDOUT, "Start Sanity Test Task 3ddddddd\n");
	char key[4] = "key\0";
	char value[4] = "123\0";
	char buf[30];
	memset(buf, 0, 30);
	if (ftag(fd, key, value) == -1) {
		printf(STDOUT, "Error1 didnt set key-value\n");
		goto exit;
	}

	// Update key
	char value2[5] = "1234\0";
	if (ftag(fd, key, value2) == -1) {
		printf(STDOUT, "Error2 didnt set key-value\n");
		goto exit;
	}

	// add new value
	char key2[5] = "yoav\0";
	char value3[5] = "omer\0";
	if (ftag(fd, key2, value3) == -1) {
		printf(STDOUT, "Error3 didnt set key-value\n");
		goto exit;
	}

	// Get value
	if (gettag(fd, key2, buf) == -1) {
		printf(STDOUT, "Error4 didnt get value\n");
		goto exit;
	} else {
		printf(STDOUT, "1.buf=%s\n", buf);
	}

	// Remove key
	funtag(fd,key);

	#ifdef DEBUG
		printf(STDOUT, "after funtag\n");
	#endif

	// fail to get key
	memset(buf, 0, 30);
	if (gettag(fd,key, buf) == -1) {
		printf(STDOUT, "Successufly failed to get key (ERROR SUCCESS)\n");
	}
	else {
		printf(STDOUT, "Failed to fail to get key (ERROR ERROR)\n");
	}

	// set key again
	if (ftag(fd, key, value) == -1) {
		printf(STDOUT, "Error didnt set key-value\n");
		goto exit;
	}
exit:
	close(fd);
	exit();
}



// void check_tags(){
// 	char buf [30];
// 	// int fd = open("./A/ct1",O_RDWR|O_CREATE);
// 	int fd2 = open("./A/B/ct2",O_RDWR|O_CREATE);
// 	//int fd2 = open("amit",O_RDWR|O_CREATE);
// 	//printf(1,"fd = %d fd2 = %d\n",fd,fd2);

// 	if (ftag(fd2,"Tyrion","Lannister") == -1){
// 		printf(1,"check_tags:Error occured! in ftag\n");
// 	}
// 	if (gettag(fd2,"Tyrion",buf) == -1){
// 		printf(1,"check_tags:Error occured! in gettag\n");
// 	}


// 	printf(1,"key = tyrion, value = %s ",buf);


// 	if (funtag(fd2,"Tyrion") == -1){
// 		printf(1,"check_tags:Error occured! in gettag\n");
// 	}

// 	if (gettag(fd2,"Tyrion",buf) == -1){
// 		printf(1,"check_tags:Tyrion we are going to see if you really shit gold\n");
// 	}

// 	if (ftag(fd,"Nedd","hand of the king") == -1){
// 		printf(1,"check_tags:Error occured! in ftag\n");
// 	}
// 	printf(1,"finished TAGGING, now going to get this tag... \n ");
// 	if (ftag(fd,"Robb","king in the north") == -1){
// 		printf(1,"check_tags:Error occured! in ftag\n");
// 	}
// 	const char* ch = "Nedd";
// 	if (gettag(fd,ch,buf) == -1){
// 		printf(1,"check_tags:Error occured! in gettag\n");
// 	}
// 	printf(1,"value of %s is %s\n",ch,buf);

// 	const char* ch2 = "Robb";
// 	if (gettag(fd,ch2 ,buf) == -1){
// 		printf(1,"check_tags:Error occured! in gettag\n");
// 	}
// 	printf(1,"value of %s is %s\n",ch2,buf);

// 	printf(1,"finished TAGGING, now going to kill the king in the north... \n ");

// 	if (funtag(fd,ch2) == -1){
// 		printf(1,"check_tags:Error occured! in funtag\n");
// 	}
// 	if (gettag(fd,ch2 ,buf) == -1){
// 		printf(1,"check_tags: there is no king in the north muahahahah\n");
// 	}

// 	if (funtag(fd,ch) == -1){
// 		printf(1,"check_tags:Error occured! in funtag\n");
// 	}

// 	if (gettag(fd,ch,buf) == -1){
// 		printf(1,"check_tags: Nedd watch your head... amm oops\n");
// 	}

// 	char* cmd[5] = {"find" , ".","-tag","Tyrion=Lannister",0};
// 	if (fork() == 0){
// 		exec(cmd[0],cmd);
// 	}
// 	wait();

// 	//close(fd);
// 	close(fd2);

// }

void file_size_test(){
	int fd = -1;
	int bytes_read = -1;
	fd = open("./check.txt", O_CREATE | O_RDWR);
	if(fd < 0){
		printf(1, "failed to open file\n");
		exit();
	}
	char *ptr_small = (char *)malloc(SMALL);
	// printf(1, "%d\n", );
	memset(ptr_small, 0x31, SMALL - 1);	
	if((bytes_read = write(fd, ptr_small, SMALL)) != SMALL){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	free(ptr_small);
	
	printf(1, "Finished writing 6KB (direct)\n");
	char *ptr_med = (char *)malloc(MEDIUM - SMALL);
	memset(ptr_med, 0x32, MEDIUM - SMALL - 1);
	if((bytes_read = write(fd, ptr_med, MEDIUM - SMALL)) != (MEDIUM - SMALL)){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	printf(1, "Finished writing 70KB (single indirect)\n");
	free(ptr_med);

	char *ptr_large = (char *)malloc(LARGE - MEDIUM);
	memset(ptr_large, 0x33, LARGE - MEDIUM - 1);
	if((bytes_read = write(fd, ptr_large, LARGE - MEDIUM)) != (LARGE - MEDIUM)){
		printf(1, "failed to write to file\n");
		printf(1, "bytes_write: %d\n", bytes_read);
		exit();
	}
	printf(1, "Finished writing 1MB (single indirect)\n");
	free(ptr_large);

	close(fd);
}
void execute(char* cmd[]){
	if (fork() == 0){
		exec(cmd[0],cmd);
	}
	wait();
}
void tag_find_test(){

	int fd = open("oz",O_CREATE|O_RDWR);
	int fd2 = open("oz2",O_CREATE|O_RDWR);
	printf(1,"******Now Create tags!******\n");

	ftag(fd,"key1","oz23");
	ftag(fd,"Nedd","Stark");
	ftag(fd2,"Leeroy","Jenkins");
	ftag(fd2,"Nedd","isDead");
 	

	char* cmd[5] = {"find" , ".","-tag","key1=oz23",0};
	char* cmd2[5] = {"find" , ".","-tag","Nedd=Stark",0};
	char* cmd3[5] = {"find" , ".","-tag","Leeroy=Jenkins",0};
	char* cmd4[5] = {"find" , ".","-tag","Nedd=?",0};

	execute(cmd);
	execute(cmd2);
	execute(cmd3);
	execute(cmd4);

	printf(1,"******Now override tags!******\n");
	ftag(fd,"key1","door1");
	ftag(fd,"Nedd","Warden");
	ftag(fd2,"Leeroy","Chicken");
	ftag(fd2,"Nedd","Bye");

	char* cmd5[5] = {"find" , ".","-tag","key1=door1",0};
	char* cmd6[5] = {"find" , ".","-tag","Nedd=Warden",0};
	char* cmd7[5] = {"find" , ".","-tag","Leeroy=Chicken",0};
	char* cmd8[5] = {"find" , ".","-tag","Nedd=?",0};

	execute(cmd5);
	execute(cmd6);
	execute(cmd7);
	execute(cmd8);

	printf(1,"******Now delete tags!******\n");

	funtag(fd,"Nedd");
	funtag(fd,"key1");
	funtag(fd2,"Nedd");
	funtag(fd2,"Leeroy");

	//should not find anything
	execute(cmd5);
	execute(cmd6);
	execute(cmd7);
	execute(cmd8); 
	 
	close(fd);


}


void task_2_test(){
	int fd;
	char *mkdir1[3] = {"mkdir", "amit", 0};
	char *mkdir2[3] = {"mkdir", "amit/amit1", 0};
	char *link1[5] = {"ln", "-s", "amit/amit1", "bb", 0};
	char *link2[5] = {"ln", "-s", "amit/amit1/panda", "cc", 0};
	char *cat1[3] = {"cat", "bb/panda", 0};
	char *cat2[3] = {"cat", "cc", 0};

	if(fork() == 0){
		exec(mkdir1[0], mkdir1);
	}
	wait();
	if(fork() == 0)
		exec(mkdir2[0], mkdir2);
	wait();
	if(fork() == 0)
		exec(link1[0], link1);
	wait();

	char *str = "FILE CONTENT!\n";
	if((fd = open("amit/amit1/panda", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}

	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);
	if(fork() == 0){
		exec(cat1[0], cat1);
	}
	wait();
	if(fork() == 0){
		exec(link2[0], link2);
	}
	wait();

	if(fork() == 0){
		exec(cat2[0], cat2);
	}
	wait();
}

int
inf_loop(){
	char *link1[5] = {"ln", "-s", "aa", "bb", 0};
	char *link2[5] = {"ln", "-s", "bb", "aa", 0};
	char *cat1[3] = {"cat", "aa", 0};
	if(fork() == 0){
		exec(link1[0], link1);
	}
	wait();	
	if(fork() == 0){
		exec(link2[0], link2);
	}
	wait();
	if(fork() == 0){
		exec(cat1[0], cat1);
	}
	wait();
	printf(1, "inf_loop: SUCCESS\n");
	return 1;
}

int
readlink_test(void){
	char *link1[5] = {"ln", "-s", "wc", "cc", 0};
	char *link2[5] = {"ln", "-s", "cc", "ee", 0};
	char *link3[5] = {"ln", "-s", "ee", "ff", 0};
	char *link4[5] = {"ln", "-s", "ff", "hh", 0};
	char *link5[5] = {"ln", "-s", "hh", "ii", 0};
	char *ls[5] = {"ls", 0};
	char buffer[BUFSIZE] = {0};

	if(fork() == 0){
		exec(link1[0], link1);
	}
	wait();

	if(fork() == 0){
		exec(link2[0], link2);
	}
	wait();

	if(fork() == 0){
		exec(link3[0], link3);
	}
	wait();

	if(fork() == 0){
		exec(link4[0], link4);
	}
	wait();

	if(fork() == 0){
		exec(link5[0], link5);
	}
	wait();

	if(fork() == 0){
		exec(ls[0], ls);
	}
	wait();

	if(readlink("ii", buffer, BUFSIZE) < 0){
		printf(1, "readlink_test: readlink failed\n");
		printf(1, "readlink_test: FAIL\n");
		return 0;
	}
	printf(1, "readlink_test: %s\n", buffer);
	if(strcmp("wc", buffer) == 0){
		printf(1, "readlink_test: SUCCESS\n");
		return 1;	
	}else{
		printf(1, "readlink_test: FAIL\n");
		return 0;
	}
}

int
loop_readlink(void){
	char *link1[5] = {"ln", "-s", "qq", "pp", 0};
	char *link2[5] = {"ln", "-s", "pp", "qq", 0};
	char buffer[BUFSIZE] = {0};
	if(fork() == 0){
		exec(link1[0], link1);
	}
	wait();	
	if(fork() == 0){
		exec(link2[0], link2);
	}
	wait();
	if(readlink("pp", buffer, BUFSIZE) < 0){
		printf(1, "readlink failed\n");
		printf(1, "loop_readlink: SUCCESS\n");
		return 1;
	}
	printf(1, "readlink_test: %s\n", buffer);
	printf(1, "loop_readlink: FAIL\n");
	return 0;
}

void build_dir_tree(void){
	char *mkdir1[3] = {"mkdir", "A", 0};
	char *mkdir2[3] = {"mkdir", "A/B", 0};
	char *mkdir3[3] = {"mkdir", "A/B/C", 0};
	char *mkdir4[3] = {"mkdir", "A/B/D", 0};
	char *mkdir5[3] = {"mkdir", "A/E", 0};
	char *mkdir6[3] = {"mkdir", "A/E/F", 0};
	char *mkdir7[3] = {"mkdir", "A/E/G", 0};
	int fd;
	char *str = "FILE CONTENT!\n";

	if(fork() == 0){
		exec(mkdir1[0], mkdir1);
	}
	wait();

	if(fork() == 0){
		exec(mkdir2[0], mkdir2);
	}
	wait();
	if(fork() == 0){
		exec(mkdir3[0], mkdir3);
	}
	wait();

	if(fork() == 0){
		exec(mkdir4[0], mkdir4);
	}
	wait();

	if(fork() == 0){
		exec(mkdir5[0], mkdir5);
	}
	wait();

	if(fork() == 0){
		exec(mkdir6[0], mkdir6);
	}
	wait();

	if(fork() == 0){
		exec(mkdir7[0], mkdir7);
	}
	wait();

	if((fd = open("A/B/C/CC", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}
	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);

	if((fd = open("A/B/D/DD", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}
	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);

	if((fd = open("A/E/F/FF", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}
	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);
	if((fd = open("A/E/G/GG", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}
	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);
}

int
long_path_readlink(void){
	char *link1[5] = {"ln", "-s", "A/B/C/CC", "rr", 0};
	char *cat1[3] = {"cat", "rr", 0};
	char buffer[BUFSIZE] = {0};
	if(fork() == 0){
		exec(link1[0], link1);
	}
	wait();
	if(fork() == 0){
		exec(cat1[0], cat1);
	}
	wait();
	if(readlink("rr", buffer, BUFSIZE) < 0){
		printf(1, "long_path_readlink: readlink failed\n");
		printf(1, "long_path_readlink: FAIL\n");
		return 0;
	}
	printf(1, "readlink_test: %s\n", buffer);
	if(strcmp(buffer, "A/B/C/CC") == 0){
		printf(1, "long_path_readlink: SUCCESS\n");
		return 1;
	} else{
		printf(1, "long_path_readlink: FAIL\n");
		return 0;
	}
}

int
ultra_long_path_readlink(void){
	char *filename = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA/panda";
	char *mkdir1[3] = {"mkdir", "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA", 0};
	char *link1[5] = {"ln", "-s", filename, "yyy", 0};
	char *cat1[5] = {"cat", filename, 0};
	char buffer[BUFSIZE] = {0};
	int fd;

	if(fork() == 0){
		exec(mkdir1[0], mkdir1);
	}
	wait();
	if(fork() == 0){
		exec(link1[0], link1);
	}
	wait();
	if(fork() == 0){
		exec(cat1[0], cat1);
	}
	wait();
	
	char *str = "FILE CONTENT!\n";
	if((fd = open(filename, O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}

	if((fd = write(fd, str, strlen(str))) < 0){
		printf(1, "write failed\n");
		exit();
	}
	close(fd);

	if(readlink("yyy", buffer, BUFSIZE) < 0){
		printf(1, "readlink failed\n");
		printf(1, "SUCCESS\n");
		return 1;
	}
	printf(1, "readlink_test: %s\n", buffer);
	return 0;
}

// void 
// yoav(void){
// 	printf(STDOUT, "Start of Sanity Test 4\n");

//     printf(STDOUT, "Symlink between ls and ls_new\n");
//     if (symlink("ls", "ls_new") < 0) {
//     	printf(STDOUT, "Failed to set symlink between root and new path\n");
//     }

//     char *args[2] = {"ls_new", 0};
//     if (fork() == 0) {
//         printf(STDOUT, "Execute ls_new: Should see ls executing\n*****\n");
//     	exec(args[0], args);
//     }
//     wait();

//     mkdir("w");
//     mkdir("w/b");

//     printf(STDOUT, "Building directories w/b, symlink w with c\n");
//     if (symlink("w", "c") < 0) {
//     	printf(STDOUT, "Failed to set symlink between root and new path\n");
//     }

//     char *args1[3] = {"ls", "c", 0};
//     if (fork() == 0) {
//         printf(STDOUT, "Execute ls on c: should see:\nc\n*****\n");
//     	exec(args1[0], args1);
//     }
//     wait();

//     mkdir("w/b/c");
//     printf(STDOUT, "Building directories w/b/c, symlink w/b with d\n");
//     if (symlink("w/b", "d") < 0) {
//         printf(STDOUT, "Failed to set symlink between root and new path\n");
//     }
//     char *args2[3] = {"ls_new", "d", 0};
//     if (fork() == 0) {
//         printf(STDOUT, "Execute ls_new on d: should see:\nd\n*****\n");
//         exec(args2[0], args2);
//     }
//     wait();

// }

void
yoav2(void){
	int fd = open("sanityFile", O_RDWR|O_CREATE);

	printf(STDOUT, "Start Sanity Test Task 3\n");
	char key[4] = "key\0";
	char value[4] = "123\0";
	char buf[30];
	memset(buf, 0, 30);
	if (ftag(fd, key, value) == -1) {
		printf(STDOUT, "Error didnt set key-value\n");
		goto exit;
	}

	// Update key
	char value2[5] = "1234\0";
	if (ftag(fd, key, value2) == -1) {
		printf(STDOUT, "Error didnt set key-value\n");
		goto exit;
	}

	// add new value
	char key2[5] = "yoav\0";
	char value3[5] = "omer\0";
	if (ftag(fd, key2, value3) == -1) {
		printf(STDOUT, "Error didnt set key-value\n");
		goto exit;
	}

	// Get value
	if (gettag(fd, key2, buf) == -1) {
		printf(STDOUT, "Error didnt get value\n");
		goto exit;
	} else {
		printf(STDOUT, "buf=%s\n", buf);
	}

	// Remove key
	funtag(fd,key);

	#ifdef DEBUG
		printf(STDOUT, "after funtag\n");
	#endif

	// fail to get key
	memset(buf, 0, 30);
	if (gettag(fd,key, buf) == -1) {
		printf(STDOUT, "Successufly failed to get key (ERROR SUCCESS)\n");
	}
	else {
		printf(STDOUT, "Failed to fail to get key (ERROR ERROR)\n");
	}

	// set key again
	if (ftag(fd, key, value) == -1) {
		printf(STDOUT, "Error didnt set key-value\n");
		goto exit;
	}
exit:
	close(fd);
	// exit();
}

void
test_find_follow(void){
	printf(1, "find .\n");
	symlink("A/B/D", "roko");
	char *find[3] = {"find", ".", 0};
	char *find_follow[4] = {"find", ".", "-follow", 0};

	if(fork() == 0){
		exec(find[0], find);
	}
	wait();

	printf(1, "\n\n\n****************************************************\n\n\n");
	sleep(20);
	printf(1, "find . -follow\n");

	if(fork() == 0){
		exec(find_follow[0], find_follow);
	}
	wait();
}

void
test_follow_lvl2(void){
	mkdir("b");
	mkdir("b/c");
	mkdir("b/c/d");
	int fd;
	// char *command[5] = {"echo", "\"panda\"", ">", "b/c/d/roz", 0};
	char *find[4] = {"find", ".", "-follow", 0};

	if((fd = open("b/c/d/roz", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}

	// if(fork() == 0){
	// 	exec(command[0], command);
	// }
	// wait();

	symlink("b/c/d", "b/link");

	if(fork() == 0){
		exec(find[0], find);
	}
	wait();
}
void
check_link_in_path_to_find(void){
	int fd;
	char *find[4] = {"find", "./link", "-follow", 0};
	mkdir("g");
	mkdir("g/h");

	if((fd = open("g/h/panda", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}
	close(fd);
	symlink("g/h", "link");

	if(fork() == 0){
		exec(find[0], find);
	}
	wait();
}

void
test_follow_22(void){
	mkdir("b");
	mkdir("b/c");
	mkdir("b/c/d");
	mkdir("b/c/d/e");
	// mkdir("b/c/d/e/f");
	// mkdir("b/c/d/e/f/g");
	int fd;
	// char *command[5] = {"echo", "\"panda\"", ">", "b/c/d/roz", 0};
	char *find[4] = {"find", ".", "-follow", 0};

	// if((fd = open("b/c/d/e/f/g/roz", O_CREATE | O_RDWR)) < 0){
	if((fd = open("b/c/d/e/roz", O_CREATE | O_RDWR)) < 0){
		printf(1, "open failed\n");
		exit();
	}

	// if(fork() == 0){
	// 	exec(command[0], command);
	// }
	// wait();

	symlink("b/c/d", "b/link");

	if(fork() == 0){
		exec(find[0], find);
	}
	wait();
}

int main(int argc, char *argv[]){
	// task_2_test();
	 //build_dir_tree();
	//check_tags();
	// tag_test();
	//test_follow_lvl2();



	//***Tests that work***
	//tag_find_test();
	// check_link_in_path_to_find();
	test_follow_22();
	//test_find_follow();


	//close(fd);
	 // int successes = 0;
	 // successes += inf_loop();
	// successes += readlink_test();
	// // successes += loop_readlink();
	// successes += long_path_readlink();
	// successes += ultra_long_path_readlink();
	// // yoav();
	// // yoav2();
	// printf(1, "\n\n\n####################################################\n\n\n");
	
	// printf(1, "%d/%d tests passed\n", successes, NUM_TESTS);

	exit();
}
