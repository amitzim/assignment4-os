#include "types.h"
#include "stat.h"
#include "user.h"
#include "fcntl.h"
#include "fs.h"



int is_valid (int fd,char* _name,int _size,int _type,int follow,char* name,int size,int sign,char* tag,char* type,char* key,char* value){
	
	char buf[30];
	if(name != 0 && strcmp(name,_name) != 0){
		// printf(1,"FM1\n");
		return 0;
	}
	if (size != 0 && sign == 43 && _size <= size){
		// printf(1,"FM2\n");
		return 0;
	}
	if (size != 0 &&  sign == 45 && _size >= size){
		// printf(1,"FM3\n");
		return 0;
	}
	if (size != 0 && sign == -1 && _size != size){
		
		// printf(1,"FM4\n");
		return 0;
	}
	//tag need to be checked.
	int num =1000;
	if(tag != 0 ){
		//printf(1,"key = '%s' ,buf = '%s' value = '%s'\n",key,buf,value);
		num = gettag(fd,key,buf);
		// printf(1,"gettag = %d\n",num);
		if (strcmp("?",value) != 0){
			if(num  == -1 || strcmp(buf,value) != 0 ){
		 		// printf(1,"FM5\n");
		 		return 0;
		 }
		}
		 else{
		 	if (num == -1)
		 		return 0;
		 }
	}
		
	//type check
	if (type != 0){
		if ((strcmp(type,"d") == 0 && _type != 1) ||(strcmp(type,"f") == 0 && _type != 2) ||(strcmp(type,"s") == 0 && _type != 4) ){
			return 0;
		}
	}
// printf(1,"fd = %d",fd);
	return 1;
}

char*
fmtname(char *path)
{
	// printf(1,"in fmtname path = %s",path);
  static char buf[DIRSIZ+1];
  char *p;

  // Find first character after last slash.
  for(p=path+strlen(path); p >= path && *p != '/'; p--)
    ;
  p++;

  // Return blank-padded name.
  if(strlen(p) >= DIRSIZ)
    return p;
  memmove(buf, p, strlen(p));
  memset(buf+strlen(p), '\0', 1);
  // memset(buf+strlen(p), ' ', DIRSIZ-strlen(p));
  return buf;
}



void seek (char* path,int follow,char* name,int size,int sign,char* tag,char* type,char* key,char* value,int first_time,int dirLink){
  
  // char buf[512], 
	char *p;
	char* buf = (char* )malloc(512);

  int fd;
  struct dirent de;
  struct stat st;
  char path_link[100];
  
  int mode = 0;
  // printf(1,"PATH = %s \n",path);

  if (follow)
  	mode = 0x020;

  if((fd = open(path, mode)) < 0){
    //printf(2, "seek0: cannot open %s\n", path);
    return;
  }
   // printf(1,"path = %s \n",path);
  if(follow && !dirLink && readlink(path, path_link, 100) != -1){
  	dirLink = 1;
  	// printf(1, "Readlink failed for: %s\n", path);
  		
  	}
  	
 
  if(fstat(fd, &st) < 0){
    printf(2, "seek1: cannot stat %s\n", path);
    close(fd);
    return;
  }

  // printf(1,"path = %s ,st.type = %d\n",path,st.type); 
  switch(st.type){
  case T_FILE:
 		// printf(1,"path = %s ,type = %d , fd = %d\n",path,st.type,fd);
  		if (is_valid (fd,fmtname(path),st.size,st.type,follow,name,size,sign,tag,type, key, value) == 1){
			if((first_time && strcmp (fmtname(path),".") == 0) ||(strcmp(fmtname(path),"..") != 0 && strcmp (fmtname(path),".") != 0))
	 			printf(1, "%s\n", path);
		}
    //printf(1, "%s %d %d %d\n", fmtname(path), st.type, st.ino, st.size);
    break;

  case T_DIR:

    // if(strlen(path) + 1 + DIRSIZ + 1 > sizeof buf){
    //   printf(1, "seek2: path too long\n");
    //   break;
    // }
  
    strcpy(buf,path);
	 // printf(1,"buf = %s\n",buf);
    p = buf+strlen(buf);
    *p++ = '/';

 	// printf(1,"hello \n");
	if (is_valid (fd,fmtname(path),st.size,st.type,follow,name,size,sign,tag,type, key, value) == 1){
		if((first_time && strcmp (fmtname(path),".") == 0) ||(strcmp(fmtname(path),"..") != 0 && strcmp (fmtname(path),".") != 0))
	 		printf(1, "%s\n", path);
	}

    while(read(fd, &de, sizeof(de)) == sizeof(de)){
  		
      // printf(1,"buf = %s\n",buf);
      if(de.inum == 0)
        continue;
      memmove(p, de.name, DIRSIZ);
      p[DIRSIZ] = 0;
      if (dirLink){
      	if(stat2(buf, &st) < 0){
        	printf(1, "seek*: cannot stat %s\n", buf);
        	continue;
     		 }
      }
      else{
      	 if(stat(buf, &st) < 0){
        		printf(1, "seek3: cannot stat %s\n", buf);
      		  continue;
     		 }
      }
     	 // printf(1,"buf = %s\n",buf);
   		 
			// printf(1,"fmtname(p) = '%s'\n",fmtname(p));
			if (strcmp(fmtname(p),".") != 0 && strcmp(fmtname(p),"..") != 0){
				// printf(1,"fmtname(p) = %s\n",fmtname(p));
					// printf(1,"fmtname(p) = '%s'\n",fmtname(p));
				if (follow && st.type == T_LNK){
					// printf(1,"Seek link\n");
					seek(buf,follow,name,size,sign,tag,type,key,value,0,1);
				}
				else{
					
					seek(buf,follow,name,size,sign,tag,type,key,value,0,dirLink);
				}
				
			}


		}
		
  
    break;
	
  case T_LNK:;
  // SymLink:
   // printf(1, "ROKO KOKO!!! path = %s\n", path);
  //printf(1, "link path = %s\n", path);
  if(follow == 1){
  	char path_in_link[80];
  	if(readlink(path, path_in_link, 80) == -1){
  		printf(1, "readlink failed for: %s\n", path);
  	}

  	//printf(1,"Path in link = %s \n",path_in_link);
	if (is_valid (fd,fmtname(path_in_link),st.size,st.type,follow,name,size,sign,tag,type, key, value) == 1){
		if((first_time && strcmp (fmtname(path_in_link),".") == 0) ||(strcmp(fmtname(path_in_link),"..") != 0 && strcmp (fmtname(path_in_link),".") != 0))
 			printf(1, "%s\n", path_in_link);
	}


  	seek(path_in_link, follow, name, size, sign, tag, type, key, value, 0,dirLink);
  }
  if (is_valid (fd,fmtname(path),st.size,st.type,follow,name,size,sign,tag,type, key, value) == 1){
	if((first_time && strcmp (fmtname(path),".") == 0) ||(strcmp(fmtname(path),"..") != 0 && strcmp (fmtname(path),".") != 0))
	 	printf(1, "%s\n", path);
	}
  break;
}
  close(fd);
}






void get_key(char* tag,int len,char* buf){
	int i  = 0;
	while (i < len){
		buf[i] = tag[i];
		i++;
	}
	buf[i-1] = '\0';
	
}


int
main(int argc, char *argv[]){

	char* path = 0;
	int i = 1;
	int follow = 0;
	char* name = 0;
	char* size = 0;
	char* type = 0;
	char* tag = 0;
	if (argc == 1){
		printf(1,"find:Not enough arguments\n");
		exit(); 
	}


	// if (argc == 2){
	// 	ls(argv[1]);
	// 	exit(); 
	// }
	path = argv[i];
	i++;
	while (i < argc){
		if (strcmp(argv[i],"-follow") == 0 ){
			follow = 1;
		}
		if (strcmp(argv[i],"-name") == 0 ){
			i++;
			name = argv[i];
		}

		if (strcmp(argv[i],"-size") == 0 ){
			i++;
			size = argv[i];
		}

		if (strcmp(argv[i],"-type") == 0 ){
			i++;
			type = argv[i];
		}

		if (strcmp(argv[i],"-tag") == 0 ){
			i++;
			tag = argv[i];
		}

		i++;
	}

	char size_sign =  -1;//(char)size[0];
	if ((size !=0 )&& ((int)size[0] == 43 || (int)size[0] == 45)){
		size_sign = (char)size[0];
		size++;
	}
	
	
	int size_num = atoi(size);

	char* after_eq = strchr(tag,'=');
	after_eq++;
	int len = strlen(tag) - strlen(after_eq);
	char buf[len];
	
 	get_key(tag,len,buf);
	
	// printf(1,"path = %s\n",path);
	// printf(1,"follow = %d\n",follow);
	// printf(1,"name = %s\n",name);
	// printf(1,"size = %s\n",size);
	// printf(1,"type = %s\n",type);
	// printf(1,"key = %s\n",buf);
	// printf(1,"value = %s\n\n",after_eq);
 // 	printf(1,"fmtname(path) = %s\n",fmtname(path));
 // 	printf (1,"tag = %s\n",tag);
 // 	printf (1,"size_num = %d\n",size_num);
 // 	printf (1,"size_sign = %d\n",size_sign);
 	 seek(path,follow,name,size_num,size_sign,tag,type,buf,after_eq,0,0);
 	// seek (char* path,int follow,char* name,int size,int sign,char* tag,char* type,char* key,char* value)
	exit();
}	