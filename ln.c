#include "types.h"
#include "stat.h"
#include "user.h"

int
main(int argc, char *argv[])
{
  switch(argc){
  	case 3:
			if(link(argv[1], argv[2]) < 0)
				printf(2, "link %s %s: failed\n", argv[1], argv[2]);
  		break;
  	case 4:
		  if(strcmp(argv[1], "-s") == 0){
		  	if(symlink(argv[2], argv[3]) < 0)
		    	printf(2, "symlink %s %s: failed\n", argv[2], argv[3]);
		  }
  		break;
  	default:
    	printf(2, "Usage: ln old new or ln -s old new\n");
  		
  }
  exit();
}
